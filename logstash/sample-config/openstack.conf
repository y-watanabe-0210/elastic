# Beats からの入力設定
input {
  beats {
    port => 5044
  }
}

# Opnestack ログの加工定義 
filter {
  # デフォルトではKeystone で使用しているapacheのログは他のオープンスタックコンポーネントとはログのフォーマットが異なる。
  # そのためif分岐で、ログ毎にフィールドに展開するよう場合分けを行う
  # Keystone Access Logの場合
  if [source] =~ /.*keystone-access.*/ {
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"message" => "%{KEYSTONE_ACCESSLOG}"}
    }

    # ログのタイムスタンプを時間軸に設定する
    date {
      match => ["[@metadata][datetime]","dd/MMM/YYYY:HH:mm:ss Z"]
    }

    # ディレクトリ名をファイルパスから_typeに設定する値を取得する
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"source" => "%{DIRECTORY_EXTRACTION}"}
    }

    # Index名を設定する
    mutate {
      add_field => {"[@metadata][index]" => "%{module}"}
    }

  # Keystone Error Logの場合
  } else if [source] =~ /.*keystone-error.*/ {
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"message" => "%{KEYSTONE_ERRORLOG}"}
    }

    mutate {
      add_field => {"[@metadata][datetime]" => "%{datetime}"}
      remove_field => ["datetime"]
    }

    # ログのタイムスタンプを時間軸に設定する
    date {
      match => ["[@metadata][datetime]","YYYY-MM-dd HH:mm:ss.SSS"]
    }

    # ディレクトリ名をファイルパスから_typeに設定する値を取得する
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"source" => "%{DIRECTORY_EXTRACTION}"}
    }

    # Index名を設定する
    mutate {
      add_field => {"[@metadata][index]" => "%{module}"}
    }
  # MariaDBの場合
  } else if [source] =~ /.*mariadb.*/ {
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"message" => "%{MARIADBLOG}"}
    }

    mutate {
      add_field => {"[@metadata][datetime]" => "%{datetime}"}
      remove_field => ["datetime"]
    }

    # ログのタイムスタンプを時間軸に設定する
    date {
      match => ["[@metadata][datetime]","YYYY-MM-dd  H:mm:ss","YYYY-MM-dd HH:mm:ss"]
    }

    # ディレクトリ名をファイルパスから_typeに設定する値を取得する
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"source" => "%{DIRECTORY_EXTRACTION}"}
    }

    # Index名を設定する
    mutate {
      add_field => {"[@metadata][index]" => "%{module}"}
    }
  # MongoDBの場合
  } else if [source] =~ /.*mongodb.*/ {
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"message" => "%{MONGODBLOG}"}
    }

    # ログのタイムスタンプを時間軸に設定する
    date {
      match => ["[@metadata][datetime]","ISO8601"]
    }

    # ディレクトリ名をファイルパスから_typeに設定する値を取得する
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"source" => "%{DIRECTORY_EXTRACTION}"}
    }

    # Index名を設定する
    mutate {
      add_field => {"[@metadata][index]" => "%{module}"}
    }
  # その他のOpenstack Components Logの場合
  } else {
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"message" => "%{STANDARD_OPENSTACKLOG}"}
    }

    # ログのタイムスタンプを時間軸に設定する
    date {
      match => ["[@metadata][datetime]","ISO8601"]
    }

    # ディレクトリ名をファイルパスから_typeに設定する値を取得する
    grok {
      patterns_dir => ["/etc/logstash/pattern"]
      match => {"source" => "%{DIRECTORY_EXTRACTION}"}
    }

    # Index名を設定する
    mutate {
      add_field => {"[@metadata][index]" => "openstack"}
    }
  }

  # インデックス名日付情報を付加する
  # @timestampフィールドから日付を取得することが可能だがUTC時刻のためプラットフォームの時刻を取得する
  ruby {
    code => "
      t = Time.now()
      event.set('[@metadata][platformdate]', t.strftime('%Y.%m.%d'))
    "
  }
}

# Elasticsearch への出力
output {
  elasticsearch {
    hosts => ["10.1.0.12","10.1.0.13","10.1.0.14"]
    index => "%{[@metadata][index]}-%{[@metadata][platformdate]}"
    document_type => "%{component}"
  }

#  stdout { codec => rubydebug }
}
