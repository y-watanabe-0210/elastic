input {
  beats {
    port => "5044"
  }
}

filter {
  if [source] =~ /\/delivery\// {
    # Extract the csv data to fields   
    csv {
      columns => [
        "report_date", "report_type", "inqure_id",
        "report_time", "branch_code", "driver_code",
        "quantity", "operation_type"
      ]  
    }

    # Translate the report type field with the dictionary information
    translate {
      field => "report_type"
      destination => "report_type_detail"
      dictionary_path => "/usr/share/logstash/data/dict/reporttype_to_detailinfo.yml"
    }

    # Translate the branch code field with the dictionary information
    translate {
      field => "branch_code"
      destination => "branch_code_detail"
      dictionary_path => "/usr/share/logstash/data/dict/branchcode_to_detailinfo.yml"
    }

    # Extract the csv fields in "reporttype_to_detailinfo"
    csv {
      source  => "report_type_detail"
      columns => [
        "operation_type", "operation"
      ]
    }

    # Extract the csv fields in "branchcode_to_detailinfo"
    csv {
      source  => "branch_code_detail"
      columns => [
        "branch_lat", "branch_lon", "branch_city", "branch_address"
      ]
    }

    # Organize the time field
    if [report_time] =~ /^\d{3}$/ {
      mutate {
        add_field => { "report_datetime" => "%{report_date}0%{report_time}" }
      }
    } else if [report_time] =~ /^\d{4}$/ {
      mutate {
        add_field => { "report_datetime" => "%{report_date}%{report_time}" }
      }
    } else if [report_time] =~ /^\d{2}$/ {
      mutate {
        add_field => { "report_datetime" => "%{report_date}00%{report_time}" }
      }
    } else if [report_time] =~ /^\d{1}$/ {
      mutate {
        add_field => { "report_datetime" => "%{report_date}000%{report_time}" }
      }
    } else {
      mutate {
        add_field => { "report_datetime" => "n/a" }
      }
    }

    # Set the GPS field
    mutate {
      add_field => { "positioning" => "%{branch_lat},%{branch_lon}" }
    }

    date {
      "match"  => ["report_datetime", "YYYYMMddHHmm"]
    }

  } else if [source] =~ /\/digital\// {
    if [source] =~ /.*\(EMS\).*/ {
      # Extract the csv data to fields   
      csv {
        columns => [
          "drove_date", "drove_time", "gps_lat", "gps_lon", 
          "elapsed_time", "g_speed", "p_speed", "g_length", "p_length",
          "positioning", "engine_rotation", "engine_rotation_pulse_time",
          "effective_speed", "dg1", "dg2", "dg3", "dg4", "dg5", "dg6", "dg7"
        ]  
      }

      # Extract the driver code
      grok {
        match => {"source" => ".*\/%{INT}\/%{INT:driver_code}\/.*csv"}
      }

      # Organize the fields
      mutate {
        add_field => {
           "drove_datetime" => "%{drove_date} %{drove_time}"
           "location" => "%{gps_lat},%{gps_lon}"
        }

        convert => { "elapsed_time" => "integer"}
        convert => { "engine_rotation" => "integer"}
        convert => { "elapsed_time" => "integer"}
        convert => { "g_length" => "integer"}
        convert => { "p_length" => "integer"}

        convert => { "g_speed" => "float"}
        convert => { "p_speed" => "float"}
      }

      # Set the @timestamp according to drove_date and time
      date {
        "match"  => ["drove_datetime", "YYYY/MM/dd HH:mm:ss"]
      }
    }
  }
}

output {
  if [source] =~ /\/digital\// {
    if [source] =~ /.*\(EMS\).*/ {
      elasticsearch {
        hosts => [ "127.0.0.1" ]
        index => "shipping-ems"
        document_type => "data"
      }

      # stdout{ codec => rubydebug }
    }
  } else if [source] =~ /\/delivery\// {
    elasticsearch {
      hosts => [ "127.0.0.1" ]
      index => "delivery"
      document_type => "data"
    }

    #stdout{ codec => rubydebug }

  } else if [source] =~ /\/other\// {
    elasticsearch {
      hosts => [ "127.0.0.1" ]
      index => "other"
      document_type => "data"
    }
  }
}
